package com.itg.template;

import android.app.Application;

import androidx.room.Room;

import com.itg.template.database.appdb.AppDB;


public class App extends Application {
    private AppDB appDB;
    private MediaManager mediaManager;

    private static App instance;
    private Storage storage;

    public Storage getStorage() {
        return storage;
    }

    public MediaManager getMediaManager() {
        return mediaManager;
    }

    public AppDB getAppDB() {
        return appDB;
    }

    public static App getInstance() {
        return instance;
    }

    public SharedPreferencesManager instanceSharePreference;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        instanceSharePreference = new SharedPreferencesManager(this);
        LanguageUtil.INSTANCE.setupLanguage(this);
        mediaManager = new MediaManager(this);
        storage = new Storage();
        appDB = Room.databaseBuilder(this, AppDB.class, "NameDB").allowMainThreadQueries().build();
    }
}
