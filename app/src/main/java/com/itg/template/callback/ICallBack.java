package com.itg.template.callback;

public interface ICallBack {
    void showFrg(String tag, Object data, boolean isBacked);
}
