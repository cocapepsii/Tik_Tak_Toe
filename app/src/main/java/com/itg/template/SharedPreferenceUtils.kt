package com.itg.template

object SharedPreferenceUtils {
    private const val LANGUAGE = "LANGUAGE"


    var languageCode: String?
        get() = App.getInstance().instanceSharePreference.getValue(LANGUAGE, null)
        set(value) = App.getInstance().instanceSharePreference.setValue(LANGUAGE, value)

}