package com.itg.template.database.appdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.itg.template.database.dao.NameDAO;
import com.itg.template.database.entities.Name;


@Database(entities = {Name.class}, version = 1, exportSchema = false)
public abstract class AppDB extends RoomDatabase {

    public abstract NameDAO getDAO();
}
