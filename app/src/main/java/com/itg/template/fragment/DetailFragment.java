package com.itg.template.fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;


import com.itg.template.App;
import com.itg.template.database.entities.Name;
import com.itg.template.viewmodel.CommonVM;
import com.symphony_tech.tiktaktoe.R;
import com.symphony_tech.tiktaktoe.databinding.FragmentDetailBinding;

import java.util.ArrayList;


public class DetailFragment extends BaseFragment<FragmentDetailBinding, CommonVM> {


    public static final String TAG = DetailFragment.class.getName();

    public DetailFragment() {
        App.getInstance().getStorage().nameList = new ArrayList<>();
    }

    @Override
    protected FragmentDetailBinding initViewBinding(LayoutInflater inflater) {
        return FragmentDetailBinding.inflate(inflater);
    }

    @Override
    protected Class<CommonVM> getClassVM() {
        return CommonVM.class;
    }

    @Override
    protected void initView() {
        App.getInstance().getStorage().nameList = App.getInstance().getAppDB().getDAO().getName();
        String[] nameList = new String[App.getInstance().getStorage().nameList.size()];
        for (int i = 0; i < App.getInstance().getStorage().nameList.size(); i++) {

            nameList[i] = App.getInstance().getStorage().nameList.get(i).getName();
        }

        if (!App.getInstance().getStorage().nameList.isEmpty()) {
            Log.e(TAG, "initView: " + App.getInstance().getStorage().nameList.size());
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, nameList);
            adapter.notifyDataSetChanged();

        } else {
            Log.e(TAG, "initView: NULLLLLLLL");
        }




        binding.playBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playClickSound();
                if (binding.textInputLayoutPlayer1.getText().toString().isEmpty() ||  binding.textInputLayoutPlayer2.getText().toString().isEmpty()) {

                    Toast.makeText(requireContext(), getString(R.string.name_is_empty), Toast.LENGTH_SHORT).show();
                    return;
                }


                App.getInstance().getStorage().playerName1 = binding.textInputLayoutPlayer1.getText().toString();
                App.getInstance().getStorage().playerName2 = binding.textInputLayoutPlayer2.getText().toString();

                App.getInstance().getStorage().playWithBot = false;
                callBack.showFrg(PlayFrg.TAG, null, false);

            }
        });

    }

    private void playClickSound() {
        App.getInstance().getMediaManager().playSound(App.getInstance().getMediaManager().CLICK_SOUND);
    }

}