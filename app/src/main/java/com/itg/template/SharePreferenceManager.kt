package com.itg.template

import android.content.Context
import android.content.SharedPreferences
import android.util.Log



class SharedPreferencesManager(val context: Context) {
    private val TAG = SharedPreferencesManager::class.java.name
    private val mPref: SharedPreferences =
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)


    companion object {
        private const val PREF_NAME = "share_pre_my_app"
    }

    fun setValue(key: String?, value: String?) {
        mPref.edit()
            .putString(key, value)
            .apply()
    }

    fun setValue(key: String?, value: Float) {
        mPref.edit()
            .putFloat(key, value)
            .apply()
    }


    fun getValue(key: String?, defau: String?): String? {
        return mPref.getString(key, defau)
    }


    fun getValue(key: String?): String? {
        return mPref.getString(key, "")
    }


    fun setIntValue(key: String?, value: Int) {
        mPref.edit()
            .putInt(key, value)
            .apply()
    }

    fun setLongValue(key: String?, value: Long) {
        mPref.edit()
            .putLong(key, value)
            .apply()
    }

    fun getLongValue(key: String?, defaultValue: Long): Long {
        return mPref.getLong(key, defaultValue)
    }

    fun getIntValue(key: String?): Int {
        return mPref.getInt(key, 0)
    }

    fun getIntValue(key: String?, defaultValue: Int): Int {
        return mPref.getInt(key, defaultValue)
    }

    fun getValue(key: String?, defaultValues: Float): Float {
        return mPref.getFloat(key, defaultValues)
    }

    fun setValueBool(key: String?, value: Boolean) {
        Log.d(TAG, "setValueBool: $key ,$value ")
        mPref.edit()
            .putBoolean(key, value)
            .apply()
    }

    fun getValueBool(key: String?): Boolean {
        return mPref.getBoolean(key, false)
    }

    fun getValueBool(key: String?, defaultValue: Boolean?): Boolean {
        return mPref.getBoolean(key, defaultValue!!)
    }

    fun remove(key: String?) {
        mPref.edit()
            .remove(key)
            .apply()
    }

    fun clear(): Boolean {
        return mPref.edit()
            .clear()
            .commit()
    }
}