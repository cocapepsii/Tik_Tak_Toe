package com.itg.template.fragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.itg.template.App;
import com.itg.template.viewmodel.CommonVM;
import com.symphony_tech.tiktaktoe.R;
import com.symphony_tech.tiktaktoe.databinding.FragmentMainBinding;


public class MainFragment extends BaseFragment<FragmentMainBinding, CommonVM> {


    public static final String TAG = MainFragment.class.getName();

    @Override
    protected FragmentMainBinding initViewBinding(LayoutInflater inflater) {
        return FragmentMainBinding.inflate(inflater);
    }

    @Override
    protected Class<CommonVM> getClassVM() {
        return CommonVM.class;
    }

    @Override
    protected void initView() {

        playAnimation();


        binding.playBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playClickSound();
                callBack.showFrg(DetailFragment.TAG, null, true);
            }
        });
        binding.playWithBot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playClickSound();
                App.getInstance().getStorage().playWithBot = true;
                callBack.showFrg(PlayFrg.TAG, null, true);
            }
        });


        binding.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playClickSound();
                callBack.showFrg(SettingFragment.TAG, null, true);
            }


        });

        binding.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playClickSound();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.let_me_recommend_you_this_application) + "https://play.google.com/store/apps/details?id=" + mContext.getPackageName() );
                mContext.startActivity(Intent.createChooser(intent, getString(R.string.share_via)));

            }
        });

        binding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playClickSound();
                Toast.makeText(mContext, "Version 1.0.0", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void playClickSound() {
        App.getInstance().getMediaManager().playSound(App.getInstance().getMediaManager().CLICK_SOUND);

    }

    private void playAnimation() {



        binding.bgText.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.swinging));
       binding.animaeRope.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.swinging));
    }
}