package com.itg.template.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Build
import android.view.View
import com.itg.template.Language
import com.itg.template.SharedPreferenceUtils
import com.itg.template.fragment.LanguageAdapter
import com.symphony_tech.tiktaktoe.R
import com.symphony_tech.tiktaktoe.databinding.ActivityLanguageBinding
import com.itg.template.LanguageUtil

class LanguageActivity : BaseActivity<ActivityLanguageBinding>() {
    companion object {
        const val OPEN_FROM_MAIN = "open_from_main"
        fun start(context: Context, clearTask: Boolean = true, openFromMain: Boolean = false) {
            val intent = Intent(context, LanguageActivity::class.java).apply {
                if (clearTask) {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
                if (openFromMain) {
                    putExtra(OPEN_FROM_MAIN, true)
                }
            }
            context.startActivity(intent)
        }
    }
    var listLanguages: ArrayList<Language> = arrayListOf()
    var languageCode = "en"
    lateinit var languageAdapter: LanguageAdapter
    private  val cameraPermission = Manifest.permission.CAMERA
    private  val notificationPermission = Manifest.permission.POST_NOTIFICATIONS

    override fun getViewBinding(): ActivityLanguageBinding =
        ActivityLanguageBinding.inflate(layoutInflater)

    override fun createView() {
        getDataLanguage()
        binding.btBack.visibility = View.VISIBLE
        binding.btSetLanguage.setOnClickListener {
            changeLanguage()
        }
        binding.btBack.setOnClickListener {
            finish()
        }

    }

    private fun initAdapter() {
        languageAdapter = LanguageAdapter(this, object : LanguageAdapter.OnItemClickListener {


            override fun onClickItemListener(item: Language) {
                if (true){
                    languageCode = item.locale
                }            }
        })
        languageAdapter.setItems(listLanguages)
        binding.listLanguage.adapter = languageAdapter
    }

    private fun getDataLanguage() {

        initData()

        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Resources.getSystem().configuration.locales.get(0)
        } else {
            Resources.getSystem().configuration.locale
        }
        var systemLanguage: Language? = null
        var position = 0


        for (language in listLanguages) {

            if (intent.getBooleanExtra(OPEN_FROM_MAIN, false)) {
                if (SharedPreferenceUtils.languageCode != null && SharedPreferenceUtils.languageCode == language.locale) {
                    systemLanguage = language
                    languageCode = language.locale
                    //   position = listLanguages.indexOf(language)
                }
            } else
                if (language.locale.equals(locale.language)) {
                    systemLanguage = language
                    languageCode = locale.language
                }
        }

        if (systemLanguage != null) {
            listLanguages.remove(systemLanguage)
            listLanguages.add(0, systemLanguage)
        }

        listLanguages[position].isChoose = true
        initAdapter()
    }

    private fun changeLanguage() {
        SharedPreferenceUtils.languageCode = languageCode

        LanguageUtil.changeLang(SharedPreferenceUtils.languageCode!!, this)
        val intent = Intent(this, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }

        startActivity(intent)
    }

    private fun initData() {
        listLanguages = ArrayList()

        listLanguages.add(
            Language(
                R.drawable.flag_vi_vietnam,
                getString(R.string.language_viet_nam),
                "vi"
            )
        )

        listLanguages.add(
            Language(
                R.drawable.flag_en_english,
                getString(R.string.language_english),
                "en"
            )
        )

    }
}