package com.itg.template.activity

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.viewbinding.ViewBinding
import com.itg.template.LanguageUtil

abstract class BaseActivity<V : ViewBinding> : AppCompatActivity() {
    private val TAG = BaseActivity::class.java.simpleName

    protected lateinit var binding: V

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepareToCreate()
        binding = getViewBinding()
        setContentView(binding.root)
        decorView = window.decorView
        hideSystemUI(window)
        createView()
        LanguageUtil.setupLanguage(this)
    }

    private fun hideSystemUI(window: Window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowCompat.setDecorFitsSystemWindows(window, false)
            WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                controller.hide(WindowInsetsCompat.Type.systemBars())
                controller.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        } else {
            // For api level 29 and before, set deprecated systemUiVisibility to the combination of all
            // flags required to put activity into immersive mode.
            @Suppress("DEPRECATION")
            val fullscreenFlags =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            @Suppress("DEPRECATION")
            window.decorView.systemUiVisibility = fullscreenFlags
        }
    }

    open fun prepareToCreate(){}

    protected abstract fun getViewBinding(): V

    protected abstract fun createView()

    private var decorView: View? = null
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
           hideSystemUI( window)
        }
    }



    open fun closeFragment() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        }
    }


    open fun actionLoadAdsSplashWhenFetchData() {

    }


}
