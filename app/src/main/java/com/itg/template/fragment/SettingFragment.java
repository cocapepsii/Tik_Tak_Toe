package com.itg.template.fragment;


import static com.itg.template.MySharePreference.SAVE_SOUND;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.itg.template.App;
import com.itg.template.MySharePreference;
import com.itg.template.activity.LanguageActivity;
import com.itg.template.activity.MainActivity;
import com.itg.template.dialog.ChessTypeDialog;
import com.itg.template.viewmodel.CommonVM;
import com.symphony_tech.tiktaktoe.R;
import com.symphony_tech.tiktaktoe.databinding.FragmentSettingBinding;


public class SettingFragment extends BaseFragment<FragmentSettingBinding, CommonVM> {
    public static final String TAG = SettingFragment.class.getName();

    @Override
    protected FragmentSettingBinding initViewBinding(LayoutInflater inflater) {
        return FragmentSettingBinding.inflate(inflater);
    }

    @Override
    protected Class<CommonVM> getClassVM() {
        return CommonVM.class;
    }

    @Override
    protected void initView() {
        setUI("ivSound", App.getInstance().getMediaManager().getSoundState());

        setClick();
    }


    private void setUI(String key, boolean state) {
        if (key.equals("ivSound")) {

            binding.btnSound.setChecked(state);
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.btn_sound) {
            playClickSound();

            if (!App.getInstance().getMediaManager().getSoundState()) {

                binding.btnSound.setChecked(true);
                App.getInstance().getMediaManager().setSoundState(true);
                MySharePreference.getInstance().setBooleanValue(SAVE_SOUND, App.getInstance().getMediaManager().getSoundState());

            } else {
                binding.btnSound.setChecked(false);
                App.getInstance().getMediaManager().setSoundState(false);
                MySharePreference.getInstance().setBooleanValue(SAVE_SOUND, App.getInstance().getMediaManager().getSoundState());
            }
        } else if (view.getId() == R.id.bt_setting_back) {
            playClickSound();
            MainActivity mainActivity = (MainActivity) mContext;
            mainActivity.onBackPressed();

        } else if (view.getId() == R.id.chestType) {
            playClickSound();
            showDialog();
        }else  if (view.getId() == R.id.language){
            Intent intent = new Intent(requireActivity(), LanguageActivity.class);
            requireActivity().startActivity(intent);
        }

    }

    private void playClickSound() {
        App.getInstance().getMediaManager().playSound(App.getInstance().getMediaManager().CLICK_SOUND);

    }

    private void showDialog() {
        ChessTypeDialog chessTypeDialog = new ChessTypeDialog(mContext);
        chessTypeDialog.show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void setClick() {
        binding.btSettingBack.setOnClickListener(this);
        binding.btnSound.setOnClickListener(this);
        binding.chestType.setOnClickListener(this);
        binding.language.setOnClickListener(this);

    }


}
