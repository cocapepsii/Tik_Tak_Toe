package com.itg.template.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import com.itg.template.Language

import com.symphony_tech.tiktaktoe.R
import com.symphony_tech.tiktaktoe.databinding.ItemLanguageBinding


class LanguageAdapter(val context: Context, val listener: OnItemClickListener) :
    BaseAdapter<Language, ItemLanguageBinding>() {

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup): ItemLanguageBinding {
        return ItemLanguageBinding.inflate(inflater, parent, false)
    }

    override fun bind(binding: ItemLanguageBinding, item: Language, position: Int) {
        binding.txtNameLanguage.text = item.title
        binding.imgIconLanguage.setImageDrawable(AppCompatResources.getDrawable(context, item.flag))

        if (item.isChoose) {
            binding.imgChooseLanguage.setImageDrawable(
                AppCompatResources.getDrawable(
                    context,
                    R.drawable.ic_ratio_button
                )
            )
        } else {
            binding.imgChooseLanguage.setImageDrawable(null)
        }
        binding.root.setOnClickListener {
            listener.onClickItemListener(item)
            for (i in listItem.indices) {
                listItem[i].isChoose = (i == listItem.indexOf(item))
            }
            notifyDataSetChanged()
        }
    }


    interface OnItemClickListener {
        fun onClickItemListener(item: Language)
    }


}